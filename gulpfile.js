const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const del = require("del");

function css() {
    return gulp
        .src("styles/style.scss")
        .pipe(sass())
        .pipe(gulp.dest("./dist/styles/"))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest("./dist/styles/"));
}

function clean() {
    return del(["dist"]);
}

function scripts() {
    return (
        gulp
            .src(["./js/**/*"])
            .pipe(gulp.dest("./dist/js/"))
    );
}
function jquery() {
    return gulp.src(['./node_modules/jquery/dist/jquery.min.js',
        'node_modules/jquery-validation/dist/jquery.validate.js'])
        .pipe(gulp.dest('./dist/js/'))
};

function html() {
    return (
        gulp
            .src(["index.html"])
            .pipe(gulp.dest("./dist/"))
    );
}

function img() {
    return (
        gulp
            .src(["./img/**/*"])
            .pipe(gulp.dest("./dist/img/"))
    );
}

function fonts() {
    return (
        gulp
            .src(["./fonts/**/*"])
            .pipe(gulp.dest("./dist/fonts/"))
    );
}

const js = gulp.series(scripts);
const build = gulp.series(clean, gulp.parallel(css,js,jquery, html, img, fonts));

exports.html = html;
exports.fonts = fonts;
exports.js = js;
exports.img = img;
exports.jquery = jquery;
exports.css = css;
exports.clean = clean;
exports.build = build;